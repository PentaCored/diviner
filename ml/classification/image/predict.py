import argparse
import json
import pathlib
from typing import Tuple

from lib.ml.image_classifier import ImageClassifier


def parse_args() -> Tuple[str, str, str, bool]:
    parser = argparse.ArgumentParser(description="Training image classification model toolkit.")
    parser.add_argument("-m", "--model-file", type=str, required=True)
    parser.add_argument("-i", "--input-file", type=str, required=True)
    parser.add_argument("-c", "--model-classes-file", type=str)
    parser.add_argument("-v", "--verbose", action="store_true")
    args = parser.parse_args()
    return args.model_file, args.input_file, args.model_classes_file, args.verbose


def run(model_file: str, input_file: str, model_classes_file: str, verbose: bool) -> None:
    model_path = pathlib.Path(model_file)
    input_path = pathlib.Path(input_file)
    image_classifier = ImageClassifier()
    image_classifier.load_model(model_path)
    class_index = image_classifier.predict_class(input_path)
    if model_classes_file:
        model_classes_path = pathlib.Path(model_classes_file)
        model_classes = json.loads(model_classes_path.read_bytes())
        print(model_classes[class_index])
    else:
        print(class_index)


def main() -> None:
    model_file, input_file, model_classes_file, verbose = parse_args()
    run(model_file, input_file, model_classes_file, verbose)


if __name__ == "__main__":
    main()
