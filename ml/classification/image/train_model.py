import argparse
import pathlib
from typing import Optional, Tuple

from lib.ml.image_classifier import ImageClassifier


def parse_args() -> Tuple[str, bool, int, bool, Optional[str]]:
    parser = argparse.ArgumentParser(description="Training image classification model toolkit.")
    parser.add_argument("-d", "--dataset-dir", type=str, required=True)
    parser.add_argument("-a", "--data-augmentation", action="store_true")
    parser.add_argument("-v", "--verbose", action="store_true")
    parser.add_argument("-e", "--epochs", type=int, default=10)
    parser.add_argument("-o", "--output-model", type=str)
    args = parser.parse_args()
    print(args)
    return args.dataset_dir, args.verbose, args.epochs, args.data_augmentation, args.output_model


def run(dataset_dir: str, verbose: bool, epochs: int, data_augmentation: bool, output_model: Optional[str] = None) -> None:
    dataset_path = pathlib.Path(dataset_dir)
    image_classifier = ImageClassifier()
    image_classifier.train_model(dataset_path, epochs=epochs, verbose=verbose, data_augmentation=data_augmentation)
    if output_model:
        model_path = pathlib.Path(output_model)
        image_classifier.save_model(model_path)


def main() -> None:
    dataset_dir, verbose, epochs, data_augmentation, output_model = parse_args()
    run(dataset_dir, verbose, epochs, data_augmentation, output_model)


if __name__ == "__main__":
    main()
