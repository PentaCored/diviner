import pathlib
from abc import ABC, abstractmethod

from tensorflow.keras.callbacks import History  # pylint: disable=no-name-in-module


class Classifier(ABC):
    @abstractmethod
    def train_model(self, dataset_path: pathlib.Path, epochs: int) -> History:
        pass

    @abstractmethod
    def save_model(self, model_path: pathlib.Path) -> None:
        pass

    @abstractmethod
    def load_model(self, model_path: pathlib.Path) -> None:
        pass

    @abstractmethod
    def predict_class(self, file_path: pathlib.Path) -> int:
        pass
