import json
import pathlib
from typing import List, Optional, Tuple

import numpy as np
import tensorflow as tf
from tensorflow.keras.callbacks import History  # pylint: disable=no-name-in-module
from tensorflow.keras.layers import Layer  # pylint: disable=no-name-in-module
from tensorflow.keras.models import Sequential  # pylint: disable=no-name-in-module
from tensorflow.python.data.ops.dataset_ops import DatasetV1Adapter, PrefetchDataset  # pylint: disable=no-name-in-module

from lib.mixins.class_logger_mixin import ClassLoggerMixin

from .classifier import Classifier


class ImageClassifier(ClassLoggerMixin, Classifier):
    IMAGE_HEIGHT: int = 128
    IMAGE_WIDTH: int = 128
    BATCH_SIZE: int = 32

    def __init__(self, model: Optional[Sequential] = None) -> None:
        super().__init__()
        self._model = Sequential()
        self._model_classes: List[str] = []

    def train_model(self, dataset_path: pathlib.Path, epochs: int = 10, data_augmentation: bool = False, verbose: bool = True) -> History:
        if self._model is None:
            self._model = self._create_model(data_augmentation=data_augmentation)

        if verbose:
            image_num = self._get_dataset_image_num(dataset_path)
            self.logger.info("Total jpg images: {}".format(image_num))

        train_ds, val_ds = self._get_datasets(dataset_path)

        self._model_classes = [class_name for class_name in train_ds.class_names]
        classes_num = len(self._model_classes)
        if verbose:
            self.logger.info("Dataset classes: {}".format(self._model_classes))

        train_ds, val_ds = self._prefetch_datasets(train_ds, val_ds)

        self._model.add(tf.keras.layers.Dense(classes_num))
        self._compile_model()

        history = self._model.fit(train_ds, validation_data=val_ds, epochs=epochs)

        if verbose:
            self._model.summary()

        return history

    def save_model(self, model_path: pathlib.Path) -> None:
        model_classes_path = pathlib.Path("{}.classes.json".format(model_path))
        self._model.save(model_path, save_format="h5")
        self._save_model_classes(model_classes_path)

    def load_model(self, model_path: pathlib.Path) -> None:
        self._model = tf.keras.models.load_model(model_path)

    def predict_class(self, file_path: pathlib.Path) -> int:
        img = tf.keras.preprocessing.image.load_img(file_path, target_size=(self.IMAGE_HEIGHT, self.IMAGE_WIDTH))
        img_array = tf.keras.preprocessing.image.img_to_array(img)
        img_array = tf.expand_dims(img_array, 0)

        predictions = self._model.predict(img_array)
        score = tf.nn.softmax(predictions[0])
        class_index: int = np.argmax(score)
        percent_confidence = 100 * np.max(score)

        print("This image most likely belongs to {} with a {:.2f} percent confidence.".format(class_index, percent_confidence))
        return class_index

    def _get_dataset_image_num(self, dataset_path: pathlib.Path) -> int:
        return len(list(dataset_path.glob("*/*.jpg")))

    def _create_model(self, data_augmentation: bool = False) -> Sequential:
        model = Sequential()
        if data_augmentation:
            model.add(self._get_data_augmentation_layer())
        for default_layer in self._get_default_layers():
            model.add(default_layer)
        return model

    def _compile_model(self) -> None:
        self._model.compile(
            optimizer="adam",
            loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
            metrics=["accuracy"],
        )

    def _get_datasets(self, dataset_path: pathlib.Path) -> Tuple[DatasetV1Adapter, DatasetV1Adapter]:
        train_ds = tf.keras.preprocessing.image_dataset_from_directory(
            dataset_path,
            validation_split=0.2,
            subset="training",
            seed=123,
            image_size=(self.IMAGE_HEIGHT, self.IMAGE_WIDTH),
            batch_size=self.BATCH_SIZE,
        )
        val_ds = tf.keras.preprocessing.image_dataset_from_directory(
            dataset_path,
            validation_split=0.2,
            subset="validation",
            seed=123,
            image_size=(self.IMAGE_HEIGHT, self.IMAGE_WIDTH),
            batch_size=self.BATCH_SIZE,
        )

        return train_ds, val_ds

    def _prefetch_datasets(self, train_ds: DatasetV1Adapter, val_ds: DatasetV1Adapter) -> Tuple[PrefetchDataset, PrefetchDataset]:
        train_ds = train_ds.cache().shuffle(1000).prefetch(buffer_size=tf.data.AUTOTUNE)
        val_ds = val_ds.cache().prefetch(buffer_size=tf.data.AUTOTUNE)
        return train_ds, val_ds

    def _get_data_augmentation_layer(self) -> Sequential:
        data_augmentation_layer = Sequential(
            [
                tf.keras.layers.experimental.preprocessing.RandomFlip("horizontal", input_shape=(self.IMAGE_HEIGHT, self.IMAGE_WIDTH, 3)),
                tf.keras.layers.experimental.preprocessing.RandomRotation(0.1),
                tf.keras.layers.experimental.preprocessing.RandomZoom(0.1),
            ],
            name="data_augmentation",
        )
        return data_augmentation_layer

    def _get_default_layers(self) -> List[Layer]:
        default_layers = [
            tf.keras.layers.experimental.preprocessing.Rescaling(1.0 / 255),
            tf.keras.layers.Conv2D(16, 3, padding="same", activation="relu"),
            tf.keras.layers.MaxPooling2D(),
            tf.keras.layers.Conv2D(32, 3, padding="same", activation="relu"),
            tf.keras.layers.MaxPooling2D(),
            tf.keras.layers.Conv2D(64, 3, padding="same", activation="relu"),
            tf.keras.layers.MaxPooling2D(),
            tf.keras.layers.Dropout(0.2),
            tf.keras.layers.Flatten(),
            tf.keras.layers.Dense(128, activation="relu"),
        ]
        return default_layers

    def _save_model_classes(self, model_classes_json_path: pathlib.Path) -> None:
        with open(model_classes_json_path, "w") as outfile:
            json.dump(self._model_classes, outfile)
