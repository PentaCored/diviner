import argparse
import os
from typing import Generator

import tensorflow as tf


def is_valid_jpeg(image_path: str) -> bool:
    try:
        image_contents = tf.io.read_file(image_path)
        tf.image.decode_jpeg(image_contents, channels=3)
    except Exception:
        return False
    return True


def iter_dataset(dir_path: str) -> Generator[str, None, None]:
    klasses = os.listdir(dir_path)
    for klass in klasses:
        klass_path = os.path.join(dir_path, klass)
        if os.path.isfile(klass_path):
            raise Exception("File in classes directory: {}".format(klass_path))
        file_list = os.listdir(klass_path)
        for file in file_list:
            file_path = os.path.join(klass_path, file)
            if os.path.isdir(file_path):
                raise Exception("Sub directory in class directory: {}".format(file_path))
            yield file_path


def print_invalid_image_dataset_files(dataset_dir: str) -> None:
    for dataset_file in iter_dataset(dataset_dir):
        if not is_valid_jpeg(dataset_file):
            print(dataset_file)


def parse_args() -> str:
    parser = argparse.ArgumentParser(description="Validate image classification dataset toolkit.")
    parser.add_argument("-d", "--dataset-dir", type=str, required=True)

    args = parser.parse_args()
    return args.dataset_dir


def main() -> None:
    dataset_dir = parse_args()
    print_invalid_image_dataset_files(dataset_dir)


if __name__ == "__main__":
    main()
