import asyncio
from asyncio.events import AbstractEventLoop
from logging.config import dictConfig
from pathlib import Path

from dependency_injector import containers, providers

from app.lib.ml.image_classifier import ImageClassifier
from app.lib.rabbitmq.rabbitmq_client import AsyncRabbitmqClient
from app.lib.rabbitmq.rabbitmq_message_processor import RabbitmqMessageProcessor
from app.lib.storage.minio_storage import MinioStorage

from app import BASE_DIR, WORKER_NAME
from app.route_handlers.tag_document_task_handler import TagDocumentTaskHandler


class Core(containers.DeclarativeContainer):

    config = providers.Configuration()

    logging = providers.Resource(
        dictConfig,
        config=config.logging,  # pylint: disable=no-member
    )

    asyncio_loop: providers.Provider[AbstractEventLoop] = providers.Resource(asyncio.get_event_loop)


class Container(containers.DeclarativeContainer):

    config = providers.Configuration(strict=True)

    core = providers.Container(
        Core,
        config=config.core,  # pylint: disable=no-member
    )

    rabbitmq_client = providers.Factory(
        AsyncRabbitmqClient,
        config.rabbitmq.RABBITMQ_DSN,  # pylint: disable=no-member
        core.asyncio_loop,  # pylint: disable=no-member
    )

    blob_storage = providers.Factory(
        MinioStorage,
        aws_endpoint=config.s3.aws_endpoint,  # pylint: disable=no-member
        aws_bucket=config.s3.aws_bucket,  # pylint: disable=no-member
        aws_access_key=config.s3.aws_access_key,  # pylint: disable=no-member
        aws_secret_key=config.s3.aws_secret_key,  # pylint: disable=no-member
    )

    # Classifier
    classifier = providers.Factory(ImageClassifier)

    # Message handlers
    message_processor = providers.Factory(RabbitmqMessageProcessor)

    tag_document_task_handler = providers.Factory(
        TagDocumentTaskHandler,
        rabbitmq_client=rabbitmq_client,
        tasks_exchange_name=config.rabbitmq.TASKS_EXCHANGE_NAME,  # pylint: disable=no-member
        blob_storage=blob_storage,
        classifier=classifier,
        model_path=providers.Resource(
            Path(BASE_DIR).joinpath,
            config.ml.model_filename,  # pylint: disable=no-member
        ),
        model_classes_path=providers.Resource(
            Path(BASE_DIR).joinpath,
            config.ml.model_classes_filename,  # pylint: disable=no-member
        ),
        worker=WORKER_NAME,
    )
