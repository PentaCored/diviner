from abc import ABC
from typing import List
from uuid import UUID

from pydantic import BaseModel

from .message import MessageABC


class TaskMessageABC(MessageABC, ABC):
    title: str


class DocumentTaskRequestData(BaseModel):
    id: int
    uuid: UUID


class DocumentTaskRequest(TaskMessageABC, ABC):
    data: DocumentTaskRequestData


class TagDocumentTaskRequest(DocumentTaskRequest):
    title: str = "tag-document"

    @staticmethod
    def routing_key() -> str:
        return "request.document.tag"


class DocumentTaskResponseData(BaseModel, ABC):
    id: int
    uuid: UUID
    results: BaseModel


class DocumentTaskResponse(TaskMessageABC, ABC):
    worker: str
    data: DocumentTaskResponseData


class TagDocumentResults(BaseModel):
    tags: List[str]


class TagDocumentTaskResponseData(DocumentTaskResponseData):
    results: TagDocumentResults


class TagDocumentTaskResponse(DocumentTaskResponse):
    title: str = "tag-document"
    data: TagDocumentTaskResponseData

    @staticmethod
    def routing_key() -> str:
        return "response.document.tag"
