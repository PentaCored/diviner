from abc import ABC, abstractmethod

from pydantic import BaseModel


class MessageABC(BaseModel, ABC):
    data: BaseModel

    @staticmethod
    @abstractmethod
    def routing_key() -> str:
        pass
