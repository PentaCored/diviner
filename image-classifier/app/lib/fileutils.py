import os
from io import IOBase
from tempfile import SpooledTemporaryFile
from typing import IO, Optional, Union

import filetype
from urllib3.response import HTTPResponse


CHUNK_SIZE = 8192


def get_content_type(data: Union[SpooledTemporaryFile, IO]) -> Optional[str]:
    position = data.tell()
    data.seek(0, os.SEEK_SET)
    content_type = filetype.guess_mime(data)
    data.seek(position, os.SEEK_SET)
    return content_type


def get_data_size(data: Union[SpooledTemporaryFile, IO]) -> int:
    if isinstance(data, SpooledTemporaryFile):
        fileno = data.fileno()
        data_size = os.fstat(fileno).st_size
        return data_size
    elif isinstance(data, IOBase):
        position = data.tell()
        data.seek(0, os.SEEK_END)
        data_size = data.tell()
        data.seek(position, os.SEEK_SET)
        return data_size
    else:
        raise TypeError("data must be an instance of SpooledTemporaryFile or IO")


def get_response_data(response: HTTPResponse) -> SpooledTemporaryFile:
    data = SpooledTemporaryFile()
    while chunk := response.read(CHUNK_SIZE):
        data.write(chunk)
    data.seek(0, os.SEEK_SET)
    return data
