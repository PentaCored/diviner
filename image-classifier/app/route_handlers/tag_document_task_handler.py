import json
from pathlib import Path
from tempfile import NamedTemporaryFile, SpooledTemporaryFile
from typing import IO, Any, List, Union

from aio_pika.message import DeliveryMode, IncomingMessage, Message

from app.lib.messaging.task import TagDocumentResults, TagDocumentTaskRequest, TagDocumentTaskResponse, TagDocumentTaskResponseData
from app.lib.mixins.class_logger_mixin import ClassLoggerMixin
from app.lib.ml.classifier import Classifier
from app.lib.rabbitmq.rabbitmq_client import AsyncRabbitmqClientABC
from app.lib.rabbitmq.rabbitmq_message_route_handler import RabbitmqMessageRouteHandlerABC
from app.lib.storage.blob_storage import BlobStorage


class TagDocumentHandlerException(Exception):
    pass


class TagDocumentTaskHandler(ClassLoggerMixin, RabbitmqMessageRouteHandlerABC):
    def __init__(
        self,
        rabbitmq_client: AsyncRabbitmqClientABC,
        tasks_exchange_name: str,
        blob_storage: BlobStorage,
        classifier: Classifier,
        model_path: Path,
        model_classes_path: Path,
        worker: str,
    ) -> None:
        super().__init__()
        self._rabbitmq_client = rabbitmq_client
        self._tasks_exchange_name = tasks_exchange_name
        self._blob_storage = blob_storage
        self._model_classes: List[str] = []
        self._classifier = classifier
        self._model_path = model_path
        self._model_classes_path = model_classes_path
        self._worker = worker
        self._load_classifier_model()
        self._load_model_classes()

    @staticmethod
    def routing_key() -> str:
        return TagDocumentTaskRequest.routing_key()

    async def handle(self, message: IncomingMessage) -> None:
        tags = []
        tag_document_task = TagDocumentTaskRequest.parse_raw(message.body)

        with self._get_task_document_data(tag_document_task) as document_data:
            document_temp_file = self._get_data_temporary_file(document_data)
        with document_temp_file:
            try:
                class_name = self._predict(document_temp_file)
                tags.append(class_name)
            except Exception:  # pylint: disable=broad-except
                self.logger.exception("Unhandled prediction exception: ")  # TODO: send to exception storage (Senty SDK)

        results = TagDocumentResults(tags=tags)
        response_data = TagDocumentTaskResponseData(
            id=tag_document_task.data.id,
            uuid=tag_document_task.data.uuid,
            results=results,
        )
        task_response = TagDocumentTaskResponse(worker=self._worker, data=response_data)

        routing_key = task_response.routing_key()
        rabbitmq_message = Message(body=task_response.json().encode(), delivery_mode=DeliveryMode.PERSISTENT)
        await self._rabbitmq_client.publish_message(
            message=rabbitmq_message,
            exchange_name=self._tasks_exchange_name,
            routing_key=routing_key,
        )

    def _load_classifier_model(self) -> None:
        self._classifier.load_model(self._model_path)

    def _load_model_classes(self) -> None:
        model_classes = json.loads(self._model_classes_path.read_bytes())
        if self._is_list_of_strings(model_classes):
            self._model_classes.clear()
            for model_class in model_classes:
                self._model_classes.append(model_class)
        else:
            raise TagDocumentHandlerException("Invalid classifier model classes format, expected List[str], loaded: {}".format(model_classes))

    def _is_list_of_strings(self, data: Any) -> bool:
        if data and isinstance(data, list):
            return all(isinstance(elem, str) for elem in data)
        else:
            return False

    def _get_task_document_data(self, task: TagDocumentTaskRequest) -> SpooledTemporaryFile:
        document_data = self._blob_storage.get(str(task.data.uuid))
        if document_data is None:
            raise TagDocumentHandlerException("Can not get task document blob data: {}".format(task))
        return document_data

    def _get_data_temporary_file(self, data: Union[SpooledTemporaryFile, IO]) -> IO:
        temporary_file = NamedTemporaryFile()
        temporary_file.write(data.read())
        temporary_file.seek(0)
        return temporary_file

    def _predict(self, temporary_file: IO) -> str:
        class_index = self._classifier.predict_class(Path(temporary_file.name))
        class_name = self._model_classes[class_index]
        return class_name
