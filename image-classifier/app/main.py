import os
import traceback
from asyncio.events import AbstractEventLoop

from app import DEFAULT_CONFIGURATION_PATH
from app.containers import Container


def run() -> None:
    container = Container()
    container.config.from_yaml(DEFAULT_CONFIGURATION_PATH, required=True, envs_required=True)
    container.core.init_resources()  # pylint: disable=no-member

    asyncio_loop: AbstractEventLoop = container.core.asyncio_loop()  # pylint: disable=no-member
    message_processor = container.message_processor()
    tag_document_task_handler = container.tag_document_task_handler()
    message_processor.add_route_handler(tag_document_task_handler)

    rabbitmq_client = container.rabbitmq_client()

    # Handle events
    asyncio_loop.run_until_complete(
        rabbitmq_client.consume_messages(
            on_message=message_processor.process,
            exchange_name=container.config.rabbitmq.TASKS_EXCHANGE_NAME(),  # pylint: disable=no-member
            routing_keys=message_processor.routing_keys,
            queue_name=container.config.image_classifier.TASKS_QUEUE_NAME(),  # pylint: disable=no-member
        )
    )

    asyncio_loop.run_forever()


def main() -> int:
    try:
        run()
    except Exception:  # pylint: disable=broad-except
        traceback.print_exc()
        return os.EX_SOFTWARE
    return os.EX_OK
