import os
import traceback
from asyncio.events import AbstractEventLoop

from aio_pika.exchange import ExchangeType

from app import DEFAULT_CONFIGURATION_PATH
from app.containers import Container


def run() -> None:
    container = Container()
    container.config.from_yaml(DEFAULT_CONFIGURATION_PATH, required=True, envs_required=True)
    container.init_resources()  # pylint: disable=no-member
    asyncio_loop: AbstractEventLoop = container.core.asyncio_loop()  # pylint: disable=no-member

    rabbitmq_client = container.rabbitmq_client()

    # Create events exchange
    asyncio_loop.run_until_complete(
        rabbitmq_client.declare_exchange(
            name=container.config.rabbitmq.EVENTS_EXCHANGE_NAME(),  # pylint: disable=no-member
            exchange_type=ExchangeType.TOPIC,
            durable=True,
        )
    )
    # Create tasks exchange
    asyncio_loop.run_until_complete(
        rabbitmq_client.declare_exchange(
            name=container.config.rabbitmq.TASKS_EXCHANGE_NAME(),  # pylint: disable=no-member
            exchange_type=ExchangeType.DIRECT,
            durable=True,
        )
    )

    # Handle events
    events_message_processor = container.message_processor()
    document_created_event_handler = container.document_created_event_handler()
    events_message_processor.add_route_handler(document_created_event_handler)
    asyncio_loop.run_until_complete(
        rabbitmq_client.consume_messages(
            on_message=events_message_processor.process,
            exchange_name=container.config.rabbitmq.EVENTS_EXCHANGE_NAME(),  # pylint: disable=no-member
            routing_keys=["#"],
            queue_name=container.config.task_scheduler.EVENTS_QUEUE_NAME(),  # pylint: disable=no-member
        )
    )

    # Handle tasks responses
    task_response_message_processor = container.message_processor()
    tag_document_task_response_handler = container.tag_document_task_response_handler()  # pylint: disable=no-member
    task_response_message_processor.add_route_handler(tag_document_task_response_handler)
    asyncio_loop.run_until_complete(
        rabbitmq_client.consume_messages(
            on_message=task_response_message_processor.process,
            exchange_name=container.config.rabbitmq.TASKS_EXCHANGE_NAME(),  # pylint: disable=no-member
            routing_keys=task_response_message_processor.routing_keys,
            queue_name=container.config.task_scheduler.TASKS_QUEUE_NAME(),  # pylint: disable=no-member
        )
    )

    asyncio_loop.run_forever()


def main() -> int:
    try:
        run()
    except Exception:  # pylint: disable=broad-except
        traceback.print_exc()
        return os.EX_SOFTWARE
    return os.EX_OK
