import os


BASE_DIR = os.path.dirname(os.path.realpath(__file__))
DEFAULT_CONFIGURATION_PATH = os.path.join(BASE_DIR, "configuration.yml")
