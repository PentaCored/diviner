from aio_pika.message import DeliveryMode, IncomingMessage, Message

from app.lib.messaging.event import DocumentCreatedEvent
from app.lib.messaging.task import DocumentTaskRequestData, TagDocumentTaskRequest
from app.lib.rabbitmq.rabbitmq_client import AsyncRabbitmqClientABC
from app.lib.rabbitmq.rabbitmq_message_route_handler import RabbitmqMessageRouteHandlerABC


class DocumentCreatedEventHandler(RabbitmqMessageRouteHandlerABC):
    def __init__(self, rabbitmq_client: AsyncRabbitmqClientABC, tasks_exchange_name: str) -> None:
        super().__init__()
        self._rabbitmq_client = rabbitmq_client
        self._tasks_exchange_name = tasks_exchange_name

    @staticmethod
    def routing_key() -> str:
        return DocumentCreatedEvent.routing_key()

    async def handle(self, message: IncomingMessage) -> None:
        document_created_event = DocumentCreatedEvent.parse_raw(message.body)
        document_task_data = DocumentTaskRequestData(
            id=document_created_event.data.id,
            uuid=document_created_event.data.uuid,
        )
        tag_document_task = TagDocumentTaskRequest(data=document_task_data)
        routing_key = tag_document_task.routing_key()
        rabbitmq_message = Message(body=tag_document_task.json().encode(), delivery_mode=DeliveryMode.PERSISTENT)
        await self._rabbitmq_client.publish_message(
            message=rabbitmq_message,
            exchange_name=self._tasks_exchange_name,
            routing_key=routing_key,
        )
