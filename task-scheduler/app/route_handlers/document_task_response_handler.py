from abc import ABC, abstractmethod

from aio_pika.message import IncomingMessage
from pydantic import BaseModel

from app.lib.api_client.api_client import ApiClientABC
from app.lib.messaging.task import DocumentTaskResponse, TagDocumentTaskResponse
from app.lib.rabbitmq.rabbitmq_message_route_handler import RabbitmqMessageRouteHandlerABC


class TaskInfo(BaseModel):
    title: str
    worker: str
    is_completed: bool
    results: BaseModel
    document_id: int


class DocumentTaskResponseHandlerABC(RabbitmqMessageRouteHandlerABC, ABC):
    def __init__(self, api_client: ApiClientABC) -> None:  # TODO: change ApiClientABC -> TaskRepositoryABC
        super().__init__()
        self._api_client = api_client

    async def handle(self, message: IncomingMessage) -> None:
        task_response = self.task_response_factory(message)
        task_info = TaskInfo(
            worker=task_response.worker,
            title=task_response.title,
            is_completed=True,
            results=task_response.data.results,
            document_id=task_response.data.id,
        )
        self._api_client.task.post(task_info.dict())

    @abstractmethod
    def task_response_factory(self, message: IncomingMessage) -> DocumentTaskResponse:
        pass


class TagDocumentTaskResponseHandler(DocumentTaskResponseHandlerABC):
    @staticmethod
    def routing_key() -> str:
        return TagDocumentTaskResponse.routing_key()

    def task_response_factory(self, message: IncomingMessage) -> DocumentTaskResponse:
        return TagDocumentTaskResponse.parse_raw(message.body)
