import asyncio
from asyncio.events import AbstractEventLoop
from logging.config import dictConfig

from dependency_injector import containers, providers

from app.lib.api_client.api_client import ApiClient
from app.lib.rabbitmq.rabbitmq_client import AsyncRabbitmqClient
from app.lib.rabbitmq.rabbitmq_message_processor import RabbitmqMessageProcessor

from app.route_handlers.document_created_event_handler import DocumentCreatedEventHandler
from app.route_handlers.document_task_response_handler import TagDocumentTaskResponseHandler


class Core(containers.DeclarativeContainer):

    config = providers.Configuration()

    logging = providers.Resource(
        dictConfig,
        config=config.logging,  # pylint: disable=no-member
    )

    asyncio_loop: providers.Provider[AbstractEventLoop] = providers.Resource(asyncio.get_event_loop)


class Container(containers.DeclarativeContainer):

    config = providers.Configuration(strict=True)

    core = providers.Container(
        Core,
        config=config.core,  # pylint: disable=no-member
    )

    rabbitmq_client = providers.Factory(
        AsyncRabbitmqClient,
        config.rabbitmq.RABBITMQ_DSN,  # pylint: disable=no-member
        core.asyncio_loop,  # pylint: disable=no-member
    )

    api_client = providers.Factory(
        ApiClient,
        config.api.API_DSN,  # pylint: disable=no-member
    )

    # Message handlers
    message_processor = providers.Factory(RabbitmqMessageProcessor)

    document_created_event_handler = providers.Factory(
        DocumentCreatedEventHandler,
        rabbitmq_client=rabbitmq_client,
        tasks_exchange_name=config.rabbitmq.TASKS_EXCHANGE_NAME,  # pylint: disable=no-member
    )

    tag_document_task_response_handler = providers.Factory(
        TagDocumentTaskResponseHandler,
        api_client=api_client,
    )
