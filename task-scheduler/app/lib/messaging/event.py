from abc import ABC
from uuid import UUID

from pydantic import BaseModel

from .message import MessageABC


class EventMessageABC(MessageABC, ABC):
    pass


class DocumentEventData(BaseModel):
    id: int
    uuid: UUID


class DocumentEvent(EventMessageABC, ABC):
    data: DocumentEventData


class DocumentCreatedEvent(DocumentEvent):
    @staticmethod
    def routing_key() -> str:
        return "document.created"


class DocumentDeletedEvent(DocumentEvent):
    @staticmethod
    def routing_key() -> str:
        return "document.deleted"
