from abc import ABC, abstractmethod

import requests


class ApiResourceClient:
    def __init__(self, api_dsn: str, path: str) -> None:
        self._api_dsn = api_dsn
        self._path = path
        self._resource_url = "{}/{}".format(self._api_dsn, self._path)

    def post(self, data: dict) -> None:
        try:
            response = requests.post(self._resource_url, json=data)
            response.raise_for_status()
        except requests.exceptions.RequestException as e:
            raise e


class ApiClientABC(ABC):
    @property
    @abstractmethod
    def task(self) -> ApiResourceClient:
        pass


class ApiClient(ApiClientABC):
    def __init__(self, api_dsn: str) -> None:
        self._api_dsn = api_dsn
        self._task = ApiResourceClient(api_dsn, "tasks")

    @property
    def task(self) -> ApiResourceClient:
        return self._task
