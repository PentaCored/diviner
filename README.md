# Getting Started with Diviner App

This project was bootstrapped with:

![HTML5](https://img.shields.io/badge/html5-%23E34F26.svg?style=for-the-badge&logo=html5&logoColor=white "HTML5")
![CSS3](https://img.shields.io/badge/css3-%231572B6.svg?style=for-the-badge&logo=css3&logoColor=white "CSS3")
![Bootstrap](https://img.shields.io/badge/bootstrap-%23563D7C.svg?style=for-the-badge&logo=bootstrap&logoColor=white "Bootstrap")
![Babel](https://img.shields.io/badge/Babel-F9DC3e?style=for-the-badge&logo=babel&logoColor=black" "Babel")
![ESLint](https://img.shields.io/badge/ESLint-4B3263?style=for-the-badge&logo=eslint&logoColor=white" "ESLint")
![ESLint](https://img.shields.io/badge/npm-CB3837?style=for-the-badge&logo=npm&logoColor=white" "ESLint")
![React](https://img.shields.io/badge/react-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB "React")
![Nginx](https://img.shields.io/badge/nginx-%23009639.svg?style=for-the-badge&logo=nginx&logoColor=white "Nginx")
![Postgres](https://img.shields.io/badge/postgres-%23316192.svg?style=for-the-badge&logo=postgresql&logoColor=white "Postgres")
![Python](https://img.shields.io/badge/python-%2314354C.svg?style=for-the-badge&logo=python&logoColor=white "Python")
![FastAPI](https://img.shields.io/badge/fastapi-109989?style=for-the-badge&logo=FASTAPI&logoColor=white "FastAPI")
![Jupyter](https://img.shields.io/badge/Jupyter-%23F37626.svg?style=for-the-badge&logo=Jupyter&logoColor=white" "Jupyter")
![TensorFlow](https://img.shields.io/badge/TensorFlow-%23FF6F00.svg?style=for-the-badge&logo=TensorFlow&logoColor=white" "TensorFlow")
![Keras](https://img.shields.io/badge/Keras-%23D00000.svg?style=for-the-badge&logo=Keras&logoColor=white "Keras")
![RabbitMQ](https://img.shields.io/badge/rabbitmq-%23FF6600.svg?&style=for-the-badge&logo=rabbitmq&logoColor=white "RabbitMQ")
![Visual Studio Code](https://img.shields.io/badge/VisualStudioCode-0078d7.svg?style=for-the-badge&logo=visual-studio-code&logoColor=white "Visual Studio Code")
![Git](https://img.shields.io/badge/git-%23F05033.svg?style=for-the-badge&logo=git&logoColor=white "Git")
![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white "Docker")

and other perfect tools such as **_BitBucket Pipelines CI_**, **_Minio S3 Storage_**.

## Available Scripts

In the project directory, you can run:
#### `make migrate`

[**Cold start required option**] Runs upgrade operations, proceeding from the current database revision to the latest.


#### `make dev`

Runs the app containers in the development mode.

Open [http://localhost/](http://localhost/) to view the Frontend App:

![Frontend App](./docs/frontend.png)

Open [http://localhost/api/v1/docs](http://localhost/api/v1/docs) to view the API docs:

![API docs](./docs/api_docs.png)

Open [http://localhost:9000/](http://localhost:9000/) to view the Minio S3 Storage:

![Minio S3 Storage](./docs/minio.png)


### Other

To view all _make_ options such as linting, prettifying, cleaning and so on just run

#### `make`

```
$ make

Diviner Makefile
Please use 'make target' where target is one of:
  help                  Show this help
  clean                 Clean some python generated cache directories and files
  lint                  Run all linters (check-isort, check-black, mypy, flake8, pylint)
  mypy                  Run the mypy tool
  check-isort           Run the isort tool in check mode only (won't modify files)
  check-black           Run the black tool in check mode only (won't modify files)
  flake8                Run the flake8 tool
  pylint                Run the pylint tool
  pretty                Run all code beautifiers (isort, black)
  isort                 Run the isort tool and update files that need to
  black                 Run the black tool and update files that need to
  check-commit          Check the validaity of the last commit message
  ci-install            Install packages to system
  create-migration      Create alembic migration
  migrate               Migrate to head version
  dev                   Up containers for development
```
