from typing import Any, List

from fastapi import BackgroundTasks, Depends, File, UploadFile

from fastapi_crudrouter import SQLAlchemyCRUDRouter
from sqlalchemy.orm import Session

from app.lib.messaging.event import DocumentCreatedEvent, DocumentDeletedEvent

from app.api import dependencies
from app.api.background_tasks import publish_event
from app.api.documents.crud import create_document, delete_all_documents, delete_document
from app.api.documents.schemas import DocumentCreate, DocumentEventOrmData, DocumentRead, DocumentUpdate
from app.models import Document


documents_router = SQLAlchemyCRUDRouter(
    schema=DocumentRead,
    create_schema=DocumentCreate,
    update_schema=DocumentUpdate,
    db_model=Document,
    db=dependencies.get_db,
    prefix="/documents",
    tags=["documents"],
    create_route=False,
    update_route=False,
)


@documents_router.post("/upload", response_model=DocumentRead, summary="Upload One")
async def upload_one(
    background_tasks: BackgroundTasks,
    db: Session = Depends(dependencies.get_db),
    document: UploadFile = File(...),
) -> Any:
    """
    Upload a document
    """
    document_name = document.filename
    document_file = document.file
    document_in = DocumentCreate(name=document_name)
    db_document = create_document(db, document_in, document_file)
    document_event_data = DocumentEventOrmData.from_orm(db_document)
    document_created_event = DocumentCreatedEvent(data=document_event_data)
    background_tasks.add_task(publish_event, document_created_event)
    return db_document


@documents_router.delete("/{item_id}", response_model=DocumentRead, summary="Delete One")
def delete_one(background_tasks: BackgroundTasks, item_id: int, db: Session = Depends(dependencies.get_db)) -> Any:
    db_document = delete_document(db, item_id)
    document_event_data = DocumentEventOrmData.from_orm(db_document)
    document_deleted_event = DocumentDeletedEvent(data=document_event_data)
    background_tasks.add_task(publish_event, document_deleted_event)
    return db_document


@documents_router.delete("", response_model=List[DocumentRead], summary="Delete All")
def delete_all(background_tasks: BackgroundTasks, db: Session = Depends(dependencies.get_db)) -> Any:
    db_documents = db.query(Document).all()
    db_documents_empty_list = delete_all_documents(db)
    for db_document in db_documents:
        document_event_data = DocumentEventOrmData.from_orm(db_document)
        document_deleted_event = DocumentDeletedEvent(data=document_event_data)
        background_tasks.add_task(publish_event, document_deleted_event)
    return db_documents_empty_list
