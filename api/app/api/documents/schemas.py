from datetime import datetime
from typing import List, Optional
from uuid import UUID

from pydantic import BaseModel

from app.lib.messaging.event import DocumentEventData

from app.api.tasks.schemas import TaskRead


# Shared properties
class DocumentBase(BaseModel):
    name: str


# Properties to receive via API on creation
class DocumentCreate(DocumentBase):
    pass


# Properties to receive via API on update
class DocumentUpdate(DocumentBase):
    pass


# Properties shared by models stored in DB
class DocumentInDBBase(DocumentBase):
    id: int
    uuid: UUID
    content_type: Optional[str] = None
    created_at: datetime
    tasks: List[TaskRead]

    class Config:
        orm_mode = True


# Additional properties to return via API
class DocumentRead(DocumentInDBBase):
    pass


# Additional properties stored in DB
class DocumentInDB(DocumentInDBBase):
    pass


class DocumentEventOrmData(DocumentEventData):
    class Config:
        orm_mode = True
