from tempfile import SpooledTemporaryFile
from typing import IO, List, Union

from fastapi_crudrouter.core import NOT_FOUND
from sqlalchemy.orm import Session

from app.lib.fileutils import get_content_type
from app.lib.storage.minio_storage import MinioStorage

from app.api.documents.schemas import DocumentCreate
from app.core.config import settings
from app.models import Document


def create_document(db: Session, document_in: DocumentCreate, document_file: Union[SpooledTemporaryFile, IO]) -> Document:
    minio_storage = MinioStorage(
        aws_endpoint=settings.AWS_ENDPOINT,
        aws_bucket=settings.AWS_BUCKET,
        aws_access_key=settings.AWS_ACCESS_KEY,
        aws_secret_key=settings.AWS_SECRET_KEY,
    )
    document_content_type = get_content_type(document_file)

    db_document = Document(**document_in.dict(), content_type=document_content_type)
    db.add(db_document)
    db.flush()
    db.refresh(db_document)

    aws_object_name = str(db_document.uuid)
    aws_object_metadata = {"document-id": db_document.id, "content-type": db_document.content_type}
    minio_storage.put(aws_object_name, document_file, db_document.content_type, metadata=aws_object_metadata)

    db.commit()
    return db_document


def delete_document(db: Session, document_id: int) -> Document:
    minio_storage = MinioStorage(
        aws_endpoint=settings.AWS_ENDPOINT,
        aws_bucket=settings.AWS_BUCKET,
        aws_access_key=settings.AWS_ACCESS_KEY,
        aws_secret_key=settings.AWS_SECRET_KEY,
    )

    db_document = db.query(Document).get(document_id)
    if not db_document:
        raise NOT_FOUND

    aws_object_name = str(db_document.uuid)
    minio_storage.delete(aws_object_name)

    db.delete(db_document)
    db.commit()
    return db_document


def delete_all_documents(db: Session) -> List[Document]:
    minio_storage = MinioStorage(
        aws_endpoint=settings.AWS_ENDPOINT,
        aws_bucket=settings.AWS_BUCKET,
        aws_access_key=settings.AWS_ACCESS_KEY,
        aws_secret_key=settings.AWS_SECRET_KEY,
    )

    minio_storage.delete_all()

    db.query(Document).delete()
    db.commit()
    db_documents = db.query(Document).all()
    return db_documents
