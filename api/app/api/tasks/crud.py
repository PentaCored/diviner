from typing import List

from fastapi_crudrouter.core import NOT_FOUND
from sqlalchemy.orm import Session

from app.api.tasks.schemas import TaskCreate
from app.models import Task


def create_task(db: Session, task_in: TaskCreate) -> Task:
    db_task = Task(**task_in.dict())
    db.add(db_task)
    db.commit()
    return db_task


def delete_task(db: Session, task_id: int) -> Task:
    db_task = db.query(Task).get(task_id)
    if not db_task:
        raise NOT_FOUND
    db.delete(db_task)
    db.commit()
    return db_task


def delete_all_tasks(db: Session) -> List[Task]:
    db.query(Task).delete()
    db.commit()
    db_tasks = db.query(Task).all()
    return db_tasks
