from datetime import datetime

from pydantic import BaseModel


# Shared properties
class TaskBase(BaseModel):
    title: str
    worker: str
    is_completed: bool
    results: dict
    document_id: int


# Properties to receive via API on creation
class TaskCreate(TaskBase):
    pass


# Properties to receive via API on update
class TaskUpdate(TaskBase):
    pass


# Properties shared by models stored in DB
class TaskInDBBase(TaskBase):
    id: int
    created_at: datetime
    updated_at: datetime

    class Config:
        orm_mode = True


# Additional properties to return via API
class TaskRead(TaskInDBBase):
    pass


# Additional properties stored in DB
class TaskInDB(TaskInDBBase):
    pass
