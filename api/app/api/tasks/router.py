from fastapi_crudrouter import SQLAlchemyCRUDRouter

from app.api import dependencies
from app.api.tasks.schemas import TaskCreate, TaskRead, TaskUpdate
from app.models import Task


tasks_router = SQLAlchemyCRUDRouter(
    schema=TaskRead,
    create_schema=TaskCreate,
    update_schema=TaskUpdate,
    db_model=Task,
    db=dependencies.get_db,
    prefix="/tasks",
    tags=["tasks"],
)
