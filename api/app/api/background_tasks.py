from aio_pika.message import DeliveryMode, Message

from app.lib.messaging.event import EventMessageABC
from app.lib.rabbitmq.rabbitmq_client import AsyncRabbitmqClient

from app.core.config import settings


async def publish_event(event: EventMessageABC) -> None:
    rabbitmq_client = AsyncRabbitmqClient(rabbitmq_dsn=settings.RABBITMQ_DSN)
    rabbitmq_message = Message(body=event.json().encode(), delivery_mode=DeliveryMode.PERSISTENT)
    routing_key = event.routing_key()
    await rabbitmq_client.publish_message(
        message=rabbitmq_message,
        exchange_name=settings.EVENTS_EXCHANGE_NAME,
        routing_key=routing_key,
    )
