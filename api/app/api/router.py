from fastapi import APIRouter

from app.api.documents.router import documents_router
from app.api.tasks.router import tasks_router


api_router = APIRouter()
api_router.include_router(documents_router)
api_router.include_router(tasks_router)
