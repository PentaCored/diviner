import uuid
from typing import TYPE_CHECKING

from sqlalchemy import Column, DateTime, Integer, String
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from app.db.base_class import Base


if TYPE_CHECKING:
    from app.models.task import Task  # noqa: F401


class Document(Base):
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True, nullable=False)
    uuid = Column(UUID(as_uuid=True), index=True, default=uuid.uuid4, nullable=False)
    content_type = Column(String, index=True)
    created_at = Column(DateTime(timezone=True), server_default=func.now(), nullable=False)
    tasks = relationship("Task", back_populates="document")
