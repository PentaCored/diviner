from typing import TYPE_CHECKING

from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer, String
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from app.db.base_class import Base


if TYPE_CHECKING:
    from app.models.document import Document  # noqa: F401


class Task(Base):
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True, nullable=False)
    worker = Column(String, index=True, nullable=False)
    is_completed = Column(Boolean(), default=False, nullable=False)
    created_at = Column(DateTime(timezone=True), server_default=func.now(), nullable=False)
    updated_at = Column(DateTime(timezone=True), server_default=func.now(), onupdate=func.now(), nullable=False)
    results = Column(JSONB)
    document_id = Column(Integer, ForeignKey("document.id"), nullable=False)
    document = relationship("Document", back_populates="tasks")
