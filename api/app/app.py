from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from app.api.router import api_router
from app.core.config import settings


app = FastAPI(title=settings.PROJECT_NAME, openapi_url=f"{settings.API_ROOT_PATH}/openapi.json")

app_origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=app_origins,
    allow_methods=["*"],
    allow_headers=["*"],
)


app.include_router(api_router, prefix=settings.API_ROOT_PATH)
