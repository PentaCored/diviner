# Import all the models, so that Base has them before being
# imported by Alembic
from app.db.base_class import Base  # noqa: F401
from app.models.document import Document  # noqa: F401
from app.models.task import Task  # noqa: F401
