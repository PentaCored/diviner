from abc import ABCMeta, abstractmethod
from tempfile import SpooledTemporaryFile
from typing import Optional


class BlobStorage:
    __metaclass__ = ABCMeta

    @abstractmethod
    def get(self, name: str) -> Optional[SpooledTemporaryFile]:
        pass

    @abstractmethod
    def put(self, name: str, spooled_file: SpooledTemporaryFile) -> None:
        pass

    @abstractmethod
    def delete(self, name: str) -> None:
        pass
