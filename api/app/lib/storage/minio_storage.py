from tempfile import SpooledTemporaryFile
from typing import IO, Optional, Union

from minio import Minio
from minio.deleteobjects import DeleteObject
from minio.helpers import ObjectWriteResult
from urllib3.response import HTTPResponse

from app.lib.fileutils import get_data_size, get_response_data
from app.lib.mixins.class_logger_mixin import ClassLoggerMixin

from .blob_storage import BlobStorage


class MinioStorage(ClassLoggerMixin, BlobStorage):
    def __init__(self, aws_endpoint: str, aws_bucket: str, aws_access_key: str, aws_secret_key: str) -> None:
        super().__init__()
        self._aws_enpoint = aws_endpoint
        self._aws_bucket = aws_bucket
        self._aws_access_key = aws_access_key
        self._aws_secret_key = aws_secret_key

    def get(self, name: str) -> Optional[SpooledTemporaryFile]:
        client = self._get_minio_client()
        try:
            response: HTTPResponse = client.get_object(self._aws_bucket, name)
            data = get_response_data(response)
            return data
        finally:
            response.close()
            response.release_conn()

    def put(
        self, name: str, data: Union[SpooledTemporaryFile, IO], content_type: Optional[str] = None, metadata: Optional[dict] = None
    ) -> ObjectWriteResult:
        data_size = get_data_size(data)
        client = self._get_minio_client()
        result = client.put_object(self._aws_bucket, name, data, data_size, content_type, metadata)
        return result

    def delete(self, name: str) -> None:
        client = self._get_minio_client()
        client.remove_object(self._aws_bucket, name)

    def delete_all(self) -> None:
        client = self._get_minio_client()
        delete_objects = (DeleteObject(x.object_name) for x in client.list_objects(self._aws_bucket, recursive=True))
        for err in client.remove_objects(self._aws_bucket, delete_objects):
            self.logger.warning("Minio deletion error: {}".format(err))
            # TODO: sendy message to sentry

    def _get_minio_client(self) -> Minio:
        client = Minio(self._aws_enpoint, access_key=self._aws_access_key, secret_key=self._aws_secret_key, secure=False)
        if not client.bucket_exists(self._aws_bucket):
            client.make_bucket(self._aws_bucket)
        return client
