import asyncio
from abc import ABC, abstractmethod
from asyncio.events import AbstractEventLoop
from typing import Any, Callable, List, Optional, Union

import aio_pika
from aio_pika.connection import Connection
from aio_pika.exchange import ExchangeType
from aio_pika.message import IncomingMessage, Message

from app.lib.mixins.class_logger_mixin import ClassLoggerMixin


class AsyncRabbitmqClientABC(ABC):
    @abstractmethod
    async def declare_exchange(self, name: str, exchange_type: str, durable: bool) -> None:
        pass

    @abstractmethod
    async def publish_message(self, message: Message, exchange_name: str, routing_key: str) -> None:
        pass

    @abstractmethod
    async def consume_messages(
        self,
        on_message: Callable[[IncomingMessage], Any],
        exchange_name: str,
        routing_keys: List[str],
        queue_name: str,
        prefetch_count: Optional[int],
    ) -> None:
        pass


class AsyncRabbitmqClient(ClassLoggerMixin, AsyncRabbitmqClientABC):
    def __init__(self, rabbitmq_dsn: str, asyncio_loop: Optional[AbstractEventLoop] = None) -> None:
        super().__init__()
        if asyncio_loop is None:
            asyncio_loop = asyncio.get_event_loop()
        self._asyncio_loop = asyncio_loop
        self._rabbitmq_dsn = rabbitmq_dsn

    async def declare_exchange(self, name: str, exchange_type: Union[ExchangeType, str] = ExchangeType.DIRECT, durable: bool = True) -> None:
        connection: Connection = await aio_pika.connect_robust(self._rabbitmq_dsn, loop=self._asyncio_loop)
        channel = await connection.channel()
        await channel.declare_exchange(name=name, type=exchange_type, durable=durable)
        await connection.close()

    async def publish_message(self, message: Message, exchange_name: str, routing_key: str) -> None:
        connection: Connection = await aio_pika.connect_robust(self._rabbitmq_dsn, loop=self._asyncio_loop)
        channel = await connection.channel()
        exchange = await channel.get_exchange(name=exchange_name)
        await exchange.publish(message=message, routing_key=routing_key)
        self.logger.info(" [x] Sent: %s.%s: %s", exchange_name, routing_key, str(message.body))
        await connection.close()

    async def consume_messages(
        self,
        on_message: Callable[[IncomingMessage], Any],
        exchange_name: str,
        routing_keys: List[str],
        queue_name: str,
        prefetch_count: Optional[int] = 1,
    ) -> None:
        connection: Connection = await aio_pika.connect_robust(self._rabbitmq_dsn, loop=self._asyncio_loop)
        channel = await connection.channel()
        await channel.set_qos(prefetch_count)
        exchange = await channel.get_exchange(name=exchange_name)
        queue = await channel.declare_queue(name=queue_name, durable=True)
        for routing_key in routing_keys:
            await queue.bind(exchange, routing_key)
        await queue.consume(self._get_consume_callback(on_message))

    def _get_consume_callback(self, on_message: Callable[[IncomingMessage], Any]) -> Callable[[IncomingMessage], Any]:
        async def _consume_callback(message: IncomingMessage) -> None:
            async with message.process(requeue=True):
                self.logger.info(" [x] Received: %s.%s: %s", message.exchange, message.routing_key, str(message.body))
                try:
                    await on_message(message)
                except Exception as e:
                    self.logger.exception(" [x] Unhandled message processing exception:")
                    raise e

        return _consume_callback
