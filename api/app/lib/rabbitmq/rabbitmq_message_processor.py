from abc import ABC, abstractmethod
from typing import Dict, List

from aio_pika.message import IncomingMessage

from app.lib.mixins.class_logger_mixin import ClassLoggerMixin

from .rabbitmq_message_route_handler import RabbitmqMessageRouteHandlerABC


class UnhandledMessage(Exception):
    pass


class TooManyRouteHandlers(Exception):
    pass


class RabbitmqMessageProcessorABC(ABC):
    @abstractmethod
    async def process(self, message: IncomingMessage) -> None:
        pass

    @abstractmethod
    def add_route_handler(self, route_handler: RabbitmqMessageRouteHandlerABC) -> None:
        pass

    @property
    @abstractmethod
    def routing_keys(self) -> List[str]:
        pass


class RabbitmqMessageProcessor(ClassLoggerMixin, RabbitmqMessageProcessorABC):
    def __init__(self) -> None:
        super().__init__()
        self._route_handlers: Dict[str, RabbitmqMessageRouteHandlerABC] = {}

    async def process(self, message: IncomingMessage) -> None:
        routing_key = message.routing_key
        try:
            await self._route_handlers[routing_key].handle(message)
        except KeyError:
            exchange = message.exchange
            error = UnhandledMessage("Unhandled exchange {} route {} for message {}".format(exchange, routing_key, message))
            self.logger.warning(str(error))

    def add_route_handler(self, route_handler: RabbitmqMessageRouteHandlerABC) -> None:
        routing_key = route_handler.routing_key()
        if routing_key in self._route_handlers.keys():
            raise TooManyRouteHandlers("Too many router handlers for route {}".format(route_handler.routing_key()))
        self._route_handlers[routing_key] = route_handler

    @property
    def routing_keys(self) -> List[str]:
        return list(self._route_handlers.keys())
