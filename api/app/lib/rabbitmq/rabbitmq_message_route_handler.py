from abc import ABC, abstractmethod

from aio_pika.message import IncomingMessage


class RabbitmqMessageRouteHandlerABC(ABC):
    @staticmethod
    @abstractmethod
    def routing_key() -> str:
        pass

    @abstractmethod
    async def handle(self, message: IncomingMessage) -> None:
        pass
