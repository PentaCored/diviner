import os
import traceback

from fastapi import FastAPI

import uvicorn

from app.app import app


def run(app: FastAPI) -> None:
    uvicorn.run(app, host="0.0.0.0", port=8000)


def main() -> int:
    # cli_args = sys.argv[1:]
    try:
        run(app)
    except Exception:  # pylint: disable=broad-except
        traceback.print_exc()
        return os.EX_SOFTWARE
    return os.EX_OK
