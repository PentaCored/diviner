import '@fortawesome/fontawesome-free/css/all.css'

import 'bootstrap/dist/css/bootstrap.min.css'
import 'jquery/dist/jquery.min.js'
import 'bootstrap/dist/js/bootstrap.min.js'

import 'admin-lte/dist/css/adminlte.css'
import 'admin-lte/dist/js/adminlte.js'

import { QueryClient, QueryClientProvider } from 'react-query'
import { ReactQueryDevtools } from 'react-query/devtools'

import DashboardLayout from './components/Layouts/Dashboard/DashboardLayout'

import './App.css'
import logo from './logo.svg'

const queryClient = new QueryClient()

const App = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <DashboardLayout isDark={true} logo={logo} title="Diviner" />
      <ReactQueryDevtools initialIsOpen={false} />
    </QueryClientProvider>
  )
}

export default App
