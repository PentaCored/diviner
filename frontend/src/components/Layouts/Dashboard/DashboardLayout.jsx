import { useEffect } from 'react'

import Header from './Header'
import Sidebar from './Sidebar'
import Documents from './Pages/Documents'

import './DashboardLayout.css'

const DASHBOARD_BODY_CLASSES = [
  'dark-mode',
  'sidebar-mini',
  'layout-fixed',
  'layout-navbar-fixed',
]
const DASHBOARD_BODY_DARK_THEME_CLASS = 'dark-mode'

const DashboardLayout = ({ isDark, logo, title }) => {
  useEffect(() => {
    document.body.classList.add(...DASHBOARD_BODY_CLASSES)
    document.body.classList.toggle(DASHBOARD_BODY_DARK_THEME_CLASS, isDark)
    return () => {
      document.body.classList.remove(...DASHBOARD_BODY_CLASSES)
    }
  }, [isDark])

  return (
    <div className="wrapper">
      <Sidebar
        logo={logo}
        isDark={isDark}
        title={title}
        activeItem="Documents"
      />
      <Header />
      <Documents />
    </div>
  )
}

export default DashboardLayout
