import classnames from 'classnames'

const NavItem = ({ href, iconClasses, title, isActive }) => {
  return (
    <li className="nav-item">
      <a href={href} className={classnames('nav-link', { active: isActive })}>
        <i className={classnames('nav-icon', iconClasses)}></i>
        <p>{title}</p>
      </a>
    </li>
  )
}

const Sidebar = ({ logo, isDark, title, activeItem }) => {
  return (
    <aside
      className={classnames('main-sidebar', 'elevation-4', {
        'sidebar-dark-primary': isDark,
      })}
    >
      <a href="/#" className="brand-link">
        <img
          src={logo}
          alt="Diviner Logo"
          className="brand-image img-circle elevation-3 wh-32px"
        />
        <span className="brand-text font-weight-light">{title}</span>
      </a>

      <div className="sidebar">
        <nav className="mt-2">
          <ul
            className="nav nav-pills nav-sidebar flex-column"
            data-widget="treeview"
            role="menu"
            data-accordion="false"
          >
            <NavItem
              href="/#"
              iconClasses={['fas', 'fa-file']}
              title="Documents"
              isActive={activeItem === 'Documents'}
            />
          </ul>
        </nav>
      </div>
    </aside>
  )
}

export default Sidebar
