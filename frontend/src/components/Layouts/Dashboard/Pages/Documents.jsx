import { useQuery, useMutation, useQueryClient } from 'react-query'
import classnames from 'classnames'

import './Documents.css'

const TOTAL_TASKS = 1

const getDocuments = () => {
  return fetch('http://127.0.0.1/api/v1/documents').then((res) => res.json())
}

const uploadDocument = (selectedFile) => {
  const formData = new FormData()
  formData.append('document', selectedFile, selectedFile.name)
  return fetch('http://127.0.0.1/api/v1/documents/upload', {
    method: 'POST',
    body: formData,
  }).then((response) => response.json())
}

const DocumentUploadForm = () => {
  const queryClient = useQueryClient()
  const mutation = useMutation(uploadDocument, {
    onSuccess: () => {
      queryClient.invalidateQueries('documents')
    },
  })
  const handleFileInput = (event) => {
    const selectedFile = event.target.files[0]
    mutation.mutate(selectedFile)
  }
  return (
    <div className="row">
      <div className="col-3">
        <div className="input-group float-left">
          <div className="custom-file">
            <input
              type="file"
              className="custom-file-input"
              onChange={handleFileInput}
            />
            <label className="custom-file-label">Choose file...</label>
          </div>
        </div>
      </div>
    </div>
  )
}

const DocumentTableItem = ({ content_type, name, created_at, tasks }) => {
  const tasks_completed_percentage = Math.floor(
    (tasks.length * 100) / TOTAL_TASKS
  )
  const progressBarStyle = {
    width: `${tasks_completed_percentage}%`,
  }

  const tags = new Set()
  for (const task of tasks) {
    if (task.title === 'tag-document' && task.is_completed) {
      for (const tag of task.results.tags) {
        tags.add(tag)
      }
    }
  }

  let document_icon = <i className="fa-2x far fa-file" />
  if (content_type && content_type.includes('image')) {
    document_icon = <i className="fa-2x far fa-file-image" />
  }

  return (
    <tr>
      <td>{document_icon}</td>
      <td>
        {name}
        {[...tags].map((tag) => {
          return (
            <span key={tag} className="badge badge-info float-right">
              {tag}
            </span>
          )
        })}
      </td>
      <td>
        <div className="row">
          <div className="col-10">
            <div className="progress progress-xs">
              <div
                className="progress-bar progress-bar-danger"
                style={progressBarStyle}
              ></div>
            </div>
          </div>
          <div className="col-2">
            {tasks.length}/{TOTAL_TASKS}
          </div>
        </div>
        <div>
          {tasks.map((task) => {
            return (
              <div key={task.id}>
                <span
                  className={classnames('badge', {
                    'badge-success': task.is_completed,
                    'badge-danger': !task.is_completed,
                  })}
                >
                  {task.worker}: {task.title}
                </span>
              </div>
            )
          })}
        </div>
      </td>
      <td>{new Date(Date.parse(created_at)).toUTCString()}</td>
    </tr>
  )
}

const DocumentsTable = () => {
  const { isLoading, error, data } = useQuery('documents', getDocuments)

  if (isLoading) {
    return <div className="row">Loading...</div>
  }
  if (error) {
    return <div className="row">{error.message}</div>
  }
  return (
    <div className="row">
      <div className="col-12">
        <div className="card documents">
          <div className="card-header">
            <h3 className="card-title">Uploaded into system</h3>
            <div className="card-tools">
              <div className="input-group input-group-sm w-160px">
                <input
                  type="text"
                  name="table_search"
                  className="form-control float-right"
                  placeholder="Search"
                />

                <div className="input-group-append">
                  <button type="submit" className="btn btn-default">
                    <i className="fas fa-search"></i>
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="card-body table-responsive p-0">
            <table className="table table-hover text-nowrap">
              <thead>
                <tr>
                  <th className="w-40px">Type</th>
                  <th>Name</th>
                  <th className="w-320px">Progress</th>
                  <th className="w-160px">Date</th>
                </tr>
              </thead>
              <tbody>
                {data.map((document) => {
                  return (
                    <DocumentTableItem
                      key={document.id}
                      content_type={document.content_type}
                      name={document.name}
                      tasks={document.tasks}
                      created_at={document.created_at}
                    />
                  )
                })}
              </tbody>
            </table>
          </div>
          <div className="card-footer clearfix">
            <DocumentUploadForm />
          </div>
        </div>
      </div>
    </div>
  )
}

const Documents = () => {
  return (
    <div className="content-wrapper">
      <div className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-12">
              <h2 className="m-0">Documents management</h2>
            </div>
          </div>
        </div>
      </div>

      <section className="content mt-2">
        <div className="container-fluid">
          <DocumentsTable />
        </div>
      </section>
    </div>
  )
}

export default Documents
