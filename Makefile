
BOLD := \033[1m
RESET := \033[0m

MAKEFLAGS += --warn-undefined-variables

API_SRC := "api/app"
TASK_SCHEDULER_SRC := "task-scheduler/app"
IMAGE_CLASSIFIER_SRC := "image-classifier/app"
ML_SRC := "ml/classification/image"

# ENV = /usr/bin/env
# SHELL := bash
# .SHELLFLAGS := -eux -o pipefail -c
# .ONESHELL:

default: help

.PHONY : help
help:  ## Show this help
	@echo "$(BOLD)Diviner Makefile$(RESET)"
	@echo "Please use 'make $(BOLD)target$(RESET)' where $(BOLD)target$(RESET) is one of:"
	@grep -h ':\s\+##' Makefile | column -tn -s# | awk -F ":" '{ print "  $(BOLD)" $$1 "$(RESET)" $$2 }'

.PHONY: clean
clean:  ## Clean some python generated cache directories and files
	@echo "$(BOLD)Cleaning$(RESET)"
	@find ./ -type d  \( -name '__pycache__' -or -name '.pytest_cache' -or -name '.mypy_cache'  \) -print0 | xargs -tr0 rm -r
	@find ./ -type f -name '*.py[cod]' -print0 | xargs -tr0 rm

.PHONY: lint
lint:  ## Run all linters (check-isort, check-black, mypy, flake8, pylint)
lint: check-isort check-black mypy flake8 pylint

.PHONY: mypy
mypy:  ## Run the mypy tool
	@echo "$(BOLD)Running mypy$(RESET)"
	@mypy "$(API_SRC)"
	@mypy "$(TASK_SCHEDULER_SRC)"
	@mypy "$(IMAGE_CLASSIFIER_SRC)"
	@mypy "$(ML_SRC)"

.PHONY: check-isort
check-isort:  ## Run the isort tool in check mode only (won't modify files)
	@echo "$(BOLD)Checking isort$(RESET)"
	@isort --check-only "$(API_SRC)" 2>&1
	@isort --check-only "$(TASK_SCHEDULER_SRC)" 2>&1
	@isort --check-only "$(IMAGE_CLASSIFIER_SRC)" 2>&1
	@isort --check-only "$(ML_SRC)" 2>&1

.PHONY: check-black
check-black:  ## Run the black tool in check mode only (won't modify files)
	@echo "$(BOLD)Checking black$(RESET)"
	@black --target-version py38 --check "$(API_SRC)" 2>&1
	@black --target-version py38 --check "$(TASK_SCHEDULER_SRC)" 2>&1
	@black --target-version py38 --check "$(IMAGE_CLASSIFIER_SRC)" 2>&1
	@black --target-version py38 --check "$(ML_SRC)" 2>&1

.PHONY: flake8
flake8:  ## Run the flake8 tool
	@echo "$(BOLD)Running flake8$(RESET)"
	@flake8 "$(API_SRC)"
	@flake8 "$(TASK_SCHEDULER_SRC)"
	@flake8 "$(IMAGE_CLASSIFIER_SRC)"
	@flake8 "$(ML_SRC)"

.PHONY: pylint
pylint:  ## Run the pylint tool
	@echo "$(BOLD)Running pylint$(RESET)"
	@pylint --errors-only "$(API_SRC)"
	@pylint --errors-only "$(TASK_SCHEDULER_SRC)"
	@pylint --errors-only "$(IMAGE_CLASSIFIER_SRC)"

.PHONY: pretty
pretty:  ## Run all code beautifiers (isort, black)
pretty: isort black

.PHONY: isort
isort:  ## Run the isort tool and update files that need to
	@echo "$(BOLD)Running isort$(RESET)"
	@isort --atomic "$(API_SRC)"
	@isort --atomic "$(TASK_SCHEDULER_SRC)"
	@isort --atomic "$(IMAGE_CLASSIFIER_SRC)"
	@isort --atomic "$(ML_SRC)"

.PHONY: black
black:  ## Run the black tool and update files that need to
	@echo "$(BOLD)Running black$(RESET)"
	@black --target-version py38 "$(API_SRC)"
	@black --target-version py38 "$(TASK_SCHEDULER_SRC)"
	@black --target-version py38 "$(IMAGE_CLASSIFIER_SRC)"
	@black --target-version py38 "$(ML_SRC)"

.PHONY: check-commit
check-commit: ## Check the validaity of the last commit message
	@echo "$(BOLD)Checking last commit message$(RESET)"
	@ci/check_commit_message.py -vl

.PHONY: ci-install
ci-install: ## Install packages to system
	@echo "$(BOLD)Installing packages to system$(RESET)"
	@pip install --upgrade pipenv
	@cd ci && pipenv install --dev --deploy --system

.PHONY: create-migration
create-migration: ## Create alembic migration
	@echo "$(BOLD)Creating alembic migration$(RESET)"
	@docker-compose -f docker-compose.dev.yml run --rm api alembic revision --autogenerate

.PHONY: migrate
migrate: ## Migrate to head version
	@echo "$(BOLD)Migrating to head version$(RESET)"
	@docker-compose -f docker-compose.dev.yml run --rm api alembic upgrade head

.PHONY: dev
dev: ## Up containers for development
	@echo "$(BOLD)Running development compose file$(RESET)"
	@docker-compose -f docker-compose.dev.yml up
